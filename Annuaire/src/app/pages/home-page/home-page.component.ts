import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  
  constructor(public userService: UserService, private router:Router){}

  public showUser(email: string): void{
    this.router.navigate(["user/" + email]);
  }

  //public title: string = 'Random Annuaire';

  ngOnInit(): void {
  }
}
