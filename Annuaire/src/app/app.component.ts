import { Component } from '@angular/core';
import axios from 'axios';
import { User } from 'src/app/models/user.model';
import { UserService } from './services/user.service';

// https://randomuser.me/api/

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent{}





  











// export class AppComponent {
//   public title: string = 'Annuaire';
//   public myvar: string = 'test'; // si private, pas dispo dans app.component.html
//   public isDisabled: boolean = true;

//   public users: User[] = []


//   public myMethod(message: string): void{
//     alert(message);
//   }

//   public toggleDisabledState(): void {
//     this.isDisabled = !this.isDisabled;
//   }

//   private privateMethod(message: string): string{
//     return message.split('').reverse().join('');
//   }
// }
