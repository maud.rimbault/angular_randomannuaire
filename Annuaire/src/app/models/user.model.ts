export interface User {
  firstName: string;
  lastName: string;
  age: number;
  email: string;
  streetNumber: number;
  streetName: string;
  postCode: number;
  city: string;
  pictureUrl: string; 
}
