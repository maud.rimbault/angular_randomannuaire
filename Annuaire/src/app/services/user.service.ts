import { Injectable } from '@angular/core';
import axios from 'axios';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public users: User[] = []; //tableau users qui contient des User (voir interface)

  public async displayNewUser(){
    const {data} = await axios.get("https://randomuser.me/api/");
    const currentData = data.results[0];
  
        this.users.push({
          firstName: currentData.name.first,
          lastName: currentData.name.last,
          age: currentData.dob.age,
          email: currentData.email,
          streetNumber: currentData.location.street.number,
          streetName: currentData.location.street.name,
          postCode: currentData.location.postcode,
          city: currentData.location.city,
          pictureUrl: currentData.picture.large,
        });
      }  
      
  constructor() { }
}
