import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LucideAngularModule, UserPlus, Gift, Building, Mail } from 'lucide-angular';
import { UserComponent } from './components/user/user.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { UserDetailPageComponent } from './pages/user-detail-page/user-detail-page.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HomePageComponent,
    UserDetailPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LucideAngularModule.pick({ UserPlus, Gift, Building, Mail })  // add all of icons that is imported.
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
